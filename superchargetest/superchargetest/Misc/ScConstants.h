//
//  ScConstants.h
//  superchargetest
//
//  Created by Ipacs Peter on 2018. 05. 02..
//  Copyright © 2018. Ipacs Peter. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef ScConstants_h
#define ScConstants_h

// Dictionary keys
static NSString* const kProgramStartDateKey      = @"start_date";
static NSString* const kProgramEndDateKey        = @"end_date";
static NSString* const kProgramTitleKey          = @"title";

static NSString* const kChannelProgrammesKey     = @"programme";
static NSString* const kChannelIdKey             = @"id";
static NSString* const kChannelTitleKey          = @"title";

#endif /* ScConstants_h */

