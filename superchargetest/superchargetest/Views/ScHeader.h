//
//  ScHeader.m
//  superchargetest
//
//  Created by Ipacs Peter on 2018. 05. 02..
//  Copyright © 2018. Ipacs Peter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScHeader : UICollectionReusableView

@property (nonatomic, strong) UILabel *titleLabel;

+ (NSString*)kind;

@end
