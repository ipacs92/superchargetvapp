//
//  ScGridLayout.m
//  superchargetest
//
//  Created by Ipacs Peter on 2018. 05. 02..
//  Copyright © 2018. Ipacs Peter. All rights reserved.
//

#import "ScGridLayout.h"
#import "ScHeader.h"

@interface ScGridLayout()
@property (nonatomic, assign) CGSize contentSize;
@property (nonatomic, strong) NSArray *cellCounts;
@property (nonatomic, strong) NSArray *sectionRects;
@property (nonatomic, assign) NSInteger sectionCount;
@end

@implementation ScGridLayout

-(void)prepareLayout {
    [super prepareLayout];
    self.sectionCount = [self.collectionView numberOfSections];
    NSMutableArray *counts = [NSMutableArray arrayWithCapacity:self.sectionCount];
    NSMutableArray *rects = [NSMutableArray arrayWithCapacity:self.sectionCount];
    for (int section = 0; section < self.sectionCount; section++) {
        [counts addObject:@([self.collectionView numberOfItemsInSection:section])];
        [rects addObject:[NSValue valueWithCGRect:CGRectMake(0, section * 50, 120 * 100, 50)]];
    }
    self.cellCounts = [NSArray arrayWithArray:counts];
    self.sectionRects = [NSArray arrayWithArray:rects];
    self.contentSize = CGSizeMake(120.0 + [self cellCountForSection:0] * 100, self.sectionCount * 50.0);
}

-(CGSize)collectionViewContentSize {
    return self.contentSize;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)path {
    UICollectionViewLayoutAttributes* attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:path];
    attributes.size = CGSizeMake(120.0, 50.0);
    CGRect sectionRect = [self.sectionRects[path.section] CGRectValue];
    attributes.center = CGPointMake(60.0 + 50.0 + path.item * 100, CGRectGetMidY(sectionRect));
    return attributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if (![kind isEqualToString:[ScHeader kind]]) return nil;
    UICollectionViewLayoutAttributes* attributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:kind withIndexPath:indexPath];
    attributes.size = CGSizeMake(120.0, 50.0);
    attributes.center = CGPointMake(attributes.size.width / 2.0, attributes.size.height / 2.0 + 50 * indexPath.section);
    return attributes;
}

- (NSInteger)cellCountForSection:(NSInteger)section {
    NSNumber *count = self.cellCounts[section];
    return [count intValue];
}

- (NSArray*)layoutAttributesForElementsInRect:(CGRect)rect {
    NSInteger page = 0;
    NSMutableArray* attributes = [NSMutableArray array];
    for (NSValue *sectionRect in self.sectionRects) {
        if (CGRectIntersectsRect(rect, sectionRect.CGRectValue)) {
            NSInteger cellCount = [self cellCountForSection:page];
            for (NSInteger i = 0; i < cellCount; i++) {
                NSIndexPath* indexPath = [NSIndexPath indexPathForItem:i inSection:page];
                [attributes addObject:[self layoutAttributesForItemAtIndexPath:indexPath]];
            }
            [attributes addObject:[self layoutAttributesForSupplementaryViewOfKind:[ScHeader kind] atIndexPath:[NSIndexPath indexPathForItem:0 inSection:page]]];
        }
        page++;
    }
    return attributes;
}
@end
