//
//  ScHeader.m
//  superchargetest
//
//  Created by Ipacs Peter on 2018. 05. 02..
//  Copyright © 2018. Ipacs Peter. All rights reserved.
//

#import "ScHeader.h"

NSString *kScHeaderKind = @"ScHeader";

@implementation ScHeader

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        self.titleLabel.backgroundColor = [UIColor whiteColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.titleLabel];
        
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0f, 49.5f, self.titleLabel.frame.size.width, 0.5f);
        bottomBorder.backgroundColor = [UIColor grayColor].CGColor;
        [self.titleLabel.layer addSublayer:bottomBorder];
    }
    return self;
}

+ (NSString*)kind {
    return (NSString*)kScHeaderKind;
}

@end
