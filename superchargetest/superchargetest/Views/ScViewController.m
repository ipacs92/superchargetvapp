//
//  ViewController.m
//  superchargetest
//
//  Created by Ipacs Peter on 2018. 05. 02..
//  Copyright © 2018. Ipacs Peter. All rights reserved.
//

#import "ScViewController.h"
#import "ScProgramCell.h"
#import "ScHeader.h"
#import "ScProgramme.h"
#import "ScChannel.h"

@interface ScViewController () <UICollectionViewDataSource>
@property (nonatomic, strong) NSMutableArray<ScChannel*> *channels;
@end

@implementation ScViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self loadChannels];
    [self.collectionView registerClass:[ScProgramCell class] forCellWithReuseIdentifier:@"ProgramCell"];
    [self.collectionView registerClass:[ScHeader class] forSupplementaryViewOfKind:[ScHeader kind] withReuseIdentifier:@"HeaderCell"];
}

- (void)loadChannels {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"example" ofType:@"json"];
    NSError *error = nil;
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:&error];
    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:JSONData options:kNilOptions error:&error];
    NSArray *channelsArray = responseDict[@"channels"];
    _channels = [NSMutableArray new];
    for (NSDictionary *currentChannelRaw in channelsArray) {
        [_channels addObject:[ScChannel objectFromDictionary:currentChannelRaw]];
    }
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.channels.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    ScChannel *currentChannel = _channels[section];
    return currentChannel.programmes.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ScProgramCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProgramCell" forIndexPath:indexPath];
    ScChannel *currentChannel = _channels[indexPath.section];
    ScProgramme *currentProgramme = currentChannel.programmes[indexPath.row];
    cell.titleLabel.text = currentProgramme.title;
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    ScHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"HeaderCell" forIndexPath:indexPath];
    ScChannel *currentChannel = _channels[indexPath.section];
    header.titleLabel.text = currentChannel.title;
    return header;
}

@end
