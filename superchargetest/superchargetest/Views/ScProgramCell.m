//
//  ScProgramCell.m
//  superchargetest
//
//  Created by Ipacs Peter on 2018. 05. 02..
//  Copyright © 2018. Ipacs Peter. All rights reserved.
//

#import "ScProgramCell.h"

@implementation ScProgramCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        self.titleLabel.backgroundColor = [UIColor lightGrayColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.titleLabel];
        
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0f, 49.5f, self.titleLabel.frame.size.width, 0.5f);
        bottomBorder.backgroundColor = [UIColor grayColor].CGColor;
        [self.titleLabel.layer addSublayer:bottomBorder];
    }
    return self;
}

@end
