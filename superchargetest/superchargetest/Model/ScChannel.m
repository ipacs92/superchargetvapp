//
//  ScChannel.m
//  superchargetest
//
//  Created by Ipacs Peter on 2018. 05. 02..
//  Copyright © 2018. Ipacs Peter. All rights reserved.
//

#import "ScChannel.h"
#import "ScProgramme.h"
#import "ScConstants.h"

@implementation ScChannel

- (id)initWithId:(NSInteger)channelId title:(NSString*)title programmes:(NSArray<ScProgramme*>*)programmes {
    if ((self = [super init])) {
        _identifier = channelId;
        _title = title;
        _programmes = programmes;
    }
    return self;
}

+ (ScChannel*)objectFromDictionary:(NSDictionary*)dictionary {
    NSArray *programmesArrayRaw = dictionary[kChannelProgrammesKey];
    NSMutableArray<ScProgramme*> *programmesArray = [NSMutableArray new];
    for (NSDictionary *currentProgramme in programmesArrayRaw) {
        [programmesArray addObject:[ScProgramme objectFromDictionary:currentProgramme]];
    }
    ScChannel *result = [[ScChannel alloc] initWithId:[[dictionary objectForKey:kChannelIdKey] integerValue]
                                                title:[dictionary objectForKey:kChannelTitleKey]
                                           programmes:programmesArray];
    return result;
}

@end
