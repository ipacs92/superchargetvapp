//
//  ScChannel.h
//  superchargetest
//
//  Created by Ipacs Peter on 2018. 05. 02..
//  Copyright © 2018. Ipacs Peter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScChannel : NSObject

@property (nonatomic, assign) NSInteger identifier;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSArray *programmes;

+ (ScChannel*)objectFromDictionary:(NSDictionary*)dictionary;

@end
