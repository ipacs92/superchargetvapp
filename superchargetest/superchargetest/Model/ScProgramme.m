//
//  ScProgramme.m
//  superchargetest
//
//  Created by Ipacs Peter on 2018. 05. 02..
//  Copyright © 2018. Ipacs Peter. All rights reserved.
//

#import "ScProgramme.h"
#import "ScConstants.h"

@implementation ScProgramme

- (id)initWithTitle:(NSString*)title startDate:(NSDate*)startDate endDate:(NSDate*)endDate {
    if ((self = [super init])) {
        _title = title;
        _startDate = startDate;
        _endDate = endDate;
    }
    return self;
}

+ (ScProgramme*)objectFromDictionary:(NSDictionary*)dictionary {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    ScProgramme *result = [[ScProgramme alloc] initWithTitle:[dictionary objectForKey:kProgramTitleKey]
                                                      startDate:[dateFormatter dateFromString:[dictionary objectForKey:kProgramStartDateKey]]
                                                        endDate:[dateFormatter dateFromString:[dictionary objectForKey:kProgramEndDateKey]]];
    return result;
}



@end
