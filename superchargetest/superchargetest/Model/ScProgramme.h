//
//  ScProgramme.h
//  superchargetest
//
//  Created by Ipacs Peter on 2018. 05. 02..
//  Copyright © 2018. Ipacs Peter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScProgramme : NSObject

@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, strong) NSString *title;

+ (ScProgramme*)objectFromDictionary:(NSDictionary*)dictionary;

@end
